const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: [
    path.resolve(__dirname, "src/js", "main.ts"),
    path.resolve(__dirname, "src/sass", "styles.scss"),
  ],
  output: {
    path: path.resolve(__dirname, "web/build/"),
    filename: "js/bundle.js",
    publicPath: "",
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
    alias: {
      vue: "vue/dist/vue.js",
    },
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)?$/,
        use: "ts-loader",
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components|vendor)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
      },
      {
        test: /\.(sass|scss|css)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].css",
              outputPath: "/css/",
            },
          },
          {
            loader: "extract-loader",
          },
          {
            loader: "css-loader",
          },
          { loader: "postcss-loader" },
          {
            loader: "sass-loader",
          },
        ],
      },
      {
        test: /\.(png|jpg|svg)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            outputPath: "../img/",
          },
        },
      },
      {
        test: /\.mp4$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "../video/",
            },
          },
        ],
      },
      {
        test: /\.(woff2?|ttf|otf|eot)$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "../fonts/",
        },
      },
    ],
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [{ from: "./src/img", to: "./img" }],
    }),
  ],
};
