module.exports = {
  important: true,
  mode: "jit",
  purge: {
    mode: "all",
    enabled: true,
    content: [
      "./src/**/*.js",
      "./templates/**/*.twig",
      "./templates/**/*.html",
      "./templates/*.html",
      "./templates/*.twig",
    ],
    options: {
      keyframes: true,
      fontFace: true,
    },
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: "transparent",
      current: "currentColor",
      blue: {
        light: "#C9E8F1",
        DEFAULT: "#76CEE4",
        dark: "#76CEE4",
      },
      white: {
        DEFAULT: "#ffffff",
      },
      grey: {
        DEFAULT: "#41474F",
      },
      yellow: {
        light: "#FFFAEC",
        DEFAULT: "#F8CD37",
      },
    },
  },
  variants: {
    extend: {},
  },
  corePlugins: {
    container: false,
  },
  plugins: [
    function ({ addComponents }) {
      addComponents({
        ".container": {
          maxWidth: "100%",
          "@screen sm": {
            maxWidth: "640px",
          },
          "@screen md": {
            maxWidth: "768px",
          },
          "@screen lg": {
            maxWidth: "1280px",
          },
          "@screen xl": {
            maxWidth: "1400px",
          },
        },
      });
    },
  ],
};
