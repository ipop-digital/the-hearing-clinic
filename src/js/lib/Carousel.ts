import Flickity from "flickity";

class Carousel {

    private _carousel: HTMLElement; 

    constructor(private _element: Element) {
        if (!_element) return;

        this._carousel = this._element.querySelector(".flickity-wrapper")

        this._init();
    }

    private _init(): void {


        var flkty = new Flickity(this._carousel, {
            contain: true,
            cellAlign: 'center',
            wrapAround: true,
            imagesLoaded: true,
            adaptiveHeight: true,
            autoPlay: true,
            pageDots: false,
            pauseAutoPlayOnHover: true,
            prevNextButtons: false
          });
        

    }
}

export default Carousel;