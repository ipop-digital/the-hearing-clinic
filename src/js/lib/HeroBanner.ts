import Glide from "@glidejs/glide";

class HeroBanner {
    
    constructor(private _element: Element) {

        if (!_element) return;
        
        this._init();

    }


    private _init(): void {

        new Glide('.o-hero-banner').mount()

    }

}

export default HeroBanner;