class Accordian {

    private _blocks: NodeList;

    constructor(private _element: HTMLElement) {
        
        if (!this._element) return;

        this._blocks = this._element.querySelectorAll(".accordian-block");

        this._init();
    }

    private _hideAll(): void {
        this._blocks.forEach( (block: HTMLElement) => {
            const content = block.querySelector(".content");

            // add active class
            block.classList.remove("active");

            if (!content.classList.contains("hidden")) {
                content.classList.add("hidden");
            }
        })
    }

    private _init(): void {
        
        this._blocks.forEach( (block:HTMLElement) => {

            // grab the elements we interact with
            const span = block.querySelector("span");
            const content = block.querySelector(".content");
            // on click
            span.addEventListener("click", () => {

                // add hidden on all accordian blocks
                this._hideAll();
                
                // show content 
                content.classList.remove("hidden");
                block.classList.add("active");

            });

        });

    }
}

export default Accordian;