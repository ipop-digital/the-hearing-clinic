class Hamburger {

    private _navigation: Element;


    constructor(private _element: Element) {

        this._init();
    
        this._navigation = document.querySelector("header nav");
    
    }

    private _init(): void {
        
        this._element.addEventListener("click", () => {

            this._element.classList.toggle("is-active");

            if (this._element.classList.contains('is-active')) {
                this._navigation.classList.remove("hidden");
            } else {
                this._navigation.classList.add("hidden");
            }

        });

    }
}


export default Hamburger;