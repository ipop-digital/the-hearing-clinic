
class PageOverlay {

    constructor(private ele: string, private timing: number = 500) {

        this.init();
    }

    private init() {
        var overlay = document.querySelector(this.ele);

        if (!overlay) return;
        overlay.classList.add("transition");

        setTimeout(function () {
            overlay.remove();
        }, this.timing)

    }
}



export default PageOverlay;