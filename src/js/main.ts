import Accordian from "./lib/Accordian";
import Carousel from "./lib/Carousel";
import Hamburger from "./lib/Hamburger";
import HeroBanner from "./lib/HeroBanner";
import PageOverlay from "./lib/PageOverlay";


window.addEventListener("load", function() {
    console.log("page loaded -- insert code below");

    const hamburger = document.querySelector(".hamburger");
    if (hamburger) {
        new Hamburger(hamburger);
    }

    // herobanner slides
    const heroBanner = document.querySelector(".o-hero-banner");
    if (heroBanner) {
        new HeroBanner(heroBanner);
    }

    // herobanner slides
    const carousels = document.querySelectorAll(".o-carousel");
    if (carousels) {
        carousels.forEach( (carousel) => {
            new Carousel(carousel);
        })
    }

    // accordian
    const accordians = document.querySelectorAll(".o-accordian");
    if (accordians) {
        accordians.forEach( (accordian:HTMLElement) => {
            new Accordian(accordian);
        })
    }


    // this is always at the bottom - no code beyond this call
    new PageOverlay(".loading-overlay", 500);
})