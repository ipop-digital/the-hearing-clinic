<?php
use craft\elements\Entry;
use craft\helpers\UrlHelper;
use aelvan\imager\variables\ImagerVariable;
use craft\elements\MatrixBlock;


$criteriaProducts = [
    'section' => 'products',
];

$criteriaNews = [
    'section' => 'news',
];

return [
    'endpoints' => [
        'news.json' => [
            'elementType' => Entry::class,
            'criteria' => $criteriaNews,
            'transformer' => function(Entry $entry) {    
                $imager = new ImagerVariable();
                $images = $entry->mainImage->all();
                $transformImages = [];
                foreach ($images as $key => $image) {
                    $transformImages [] = [
                        'url' => 'http://ipop.je/stjohn/web/' . $imager->transformImage( $image , [ 'width' => '1500', 'blur' => true ] )->getUrl()
                        // 'url' => 'https://www.stjohn.com' . $imager->transformImage( $image , ['width' => '1500', 'blur' => true ] )->getUrl(),                        
                        // 'url' => 'http://localhost/stjohn/web/' . $imager->transformImage( $image , [ 'width' => '500', 'blur' => true ] )->getUrl(),
                        // 'placeholder' => 'http://localhost/stjohn/web/' . $imager->transformImage( $image , [ 'width' => '50', 'blur' => true ] )->getUrl(),
                        // 'placeholder' => 'http://ipop.je/stjohn/web/' . $imager->transformImage( $image , [ 'width' => '6', 'blur' => true ] )->getUrl(),
                        // 'placeholder' => 'https://www.stjohn.com' . $imager->transformImage( $image , [ 'width' => '6', 'blur' => true ] )->getUrl(),                        
                    ];
                }           
                return [
                    'title' => $entry->title,
                    'url' => $entry->url,                    
                    'postDate' => $entry->postDate,                                        
                    'images' => $transformImages,                    
                ];
            },        
        ],
        'products.json' => [
            'elementType' => Entry::class,
            'criteria' => $criteriaProducts,
            'transformer' => function(Entry $entry) {    
                $imager = new ImagerVariable();
                if ($entry->product){
                    foreach ($entry->product as $block) {
                        $images = $block->images->all();
                        $transformImages = [];
                        // foreach ($images as $key => $image) {
                        //     $transformImages [] = [
                        //         // 'url' => 'https://www.stjohn.com' . $imager->transformImage( $image , ['width' => '1500', 'blur' => true ] )->getUrl(),
                        //         // 'url' => 'http://localhost/stjohn/web/' . $imager->transformImage( $image , [ 'width' => '1500', 'blur' => true ] )->getUrl()
                        //         'url' => 'http://ipop.je/stjohn/web/' . $imager->transformImage( $image , [ 'width' => '1500', 'blur' => true ] )->getUrl()
                        //         // 'placeholder' => 'http://localhost/stjohn/web/' . $imager->transformImage( $image , [ 'width' => '50', 'blur' => true ] )->getUrl(),                                
                        //         // 'placeholder' => 'http://ipop.je/stjohn/web/' . $imager->transformImage( $image , [ 'width' => '6', 'blur' => true ] )->getUrl(),
                        //         // 'placeholder' => 'https://www.stjohn.com' . $imager->transformImage( $image , [ 'width' => '6', 'blur' => true ] )->getUrl(),
                        //     ];
                        // }                                                     
                    }   
                }
                
                return [
                    'title' => $entry->title,
                    'url' => $entry->url,                    
                    'postDate' => $entry->postDate,                                        
                    'images' => $images,   
                    'category' => $entry->productCategory->all()             
                ];
            },        
        ],
    ]
];