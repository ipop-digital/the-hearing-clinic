const { resolve } = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    output: {
        path: resolve(__dirname, 'public_html/build/'),
        filename: 'js/bundle.js',
        publicPath: ""
    },
    devtool: "nosources-source-map",
    mode: "production"
});